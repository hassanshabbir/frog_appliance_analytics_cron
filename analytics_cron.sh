#!/bin/bash

function getDBName {
	echo $(mysql -u$MYSQL_USER -p$MYSQL_USER_PW -e "SELECT SCHEMA_NAME FROM information_schema.SCHEMATA WHERE schema_name LIKE 'frogos%'"|grep frogos)
}

function getLogins {
	`mysql -u$MYSQL_USER -p$MYSQL_USER_PW $DBNAME -e "SELECT frogos_user.username, fuc.login_time,fuc.login_time,fuc.session_length FROM frogos_analytics_user_cache fuc INNER JOIN frogos_user ON frogos_user.uuid = fuc.user_uuid INTO OUTFILE '$DIRECTORY/logins/logins.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"`
	return $?
}

function getAssignments {
	`mysql -u$MYSQL_USER -p$MYSQL_USER_PW $DBNAME -e "SELECT frogos_user.username, frogos_site.name, frogos_subject.name as subject, SUM(frogos_analytics_assignment_cache.total_assignees) as assignees, (SELECT COUNT(*) FROM frogos_assignment_user_info WHERE frogos_assignment_user_info.assignment_uuid = frogos_analytics_assignment_cache.assignment_uuid AND frogos_assignment_user_info.completed IS NOT NULL) AS users_completed FROM frogos_analytics_assignment_cache inner JOIN frogos_site ON frogos_site.uuid = frogos_analytics_assignment_cache.assignment_uuid inner JOIN frogos_user ON frogos_user.uuid = frogos_analytics_assignment_cache.author_uuid left JOIN frogos_subject ON frogos_subject.uuid = frogos_analytics_assignment_cache.subject_uuid WHERE frogos_analytics_assignment_cache.date = DATE_SUB(CURDATE(),INTERVAL 1  DAY) GROUP BY frogos_analytics_assignment_cache.assignment_uuid INTO OUTFILE '$DIRECTORY/assignments/assignments.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"`
	return $?
}

function getSites {
	`mysql -u$MYSQL_USER -p$MYSQL_USER_PW $DBNAME -e "SELECT  fs.name, fs.description, fs.created, fs.type,fu.username FROM frogos_site fs INNER JOIN frogos_user fu on fs.creator_uuid = fu.uuid ORDER BY fs.name ASC INTO OUTFILE '$DIRECTORY/sites/sites.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'"`
}

TIMESTAMP=`date +%H:%M:%S:%Y-%m-%d`
DATE=`date +%d-%m-%Y`
LOG="/var/log/analytics_cron.log"
DIRECTORY="/srv/analytics/$DATE"

MYSQL_USER="root"
MYSQL_USER_PW="MalaysiaPilot109"

if [ ! -d "$DIRECTORY" ]; then
	echo "[$TIMESTAMP] analytics directory for today's date doesn't exist creating it .." 
	`mkdir -p /srv/analytics/$DATE && chown -R mysql: /srv/analytics`

	#fetch database name from MySQL
	DBNAME=$(getDBName)

	if [ ! -d "$DIRECTORY/logins" ]; then
		echo "[$TIMESTAMP] logins directory doesn't exist creating it .."
		`mkdir -p $DIRECTORY/logins && chown -R mysql: $DIRECTORY/logins`
		GETLOGINS_RETURNED=$(getLogins)
		echo "Exit Code -> $GETLOGINS_RETURNED"

	else
		echo "[$TIMESTAMP] logins directory already exists no need to re-create EXITING .."
		exit 1
	fi

	if [ ! -d "$DIRECTORY/assignments" ]; then
		echo "[$TIMESTAMP] assignments directory doesn't exist creating it .."
		`mkdir -p $DIRECTORY/assignments && chown -R mysql: $DIRECTORY/assignments`
		GETASSIGNMENTS_RETURNED=$(getAssignments)
		echo "Exit Code -> $GETASSIGNMENTS_RETURNED"
	else
		echo "[$TIMESTAMP] assignments directory already exists no need to re-create EXITING .."
		exit 1
	fi
	
	if [ ! -d "$DIRECTORY/sites" ]; then
		echo "[$TIMESTAMP] sites directory doesn't exist creating it .."
		`mkdir -p $DIRECTORY/sites && chown -R mysql: $DIRECTORY/sites`
		GETSITES_RETURNED=$(getSites)
		echo "Exit Code -> $GETSITES_RETURNED"
	else
		echo "[$TIMESTAMP] sites directory already exists no need to re-create EXITING .."
		exit 1
	fi
else
	echo "[$TIMESTAMP] analytics directory for today's date exists no need to re-create it EXITING .." 
	exit 1
fi
